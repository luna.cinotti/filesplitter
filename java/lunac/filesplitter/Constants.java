package lunac.filesplitter;

/**
 * Costanti pubbliche visibili da qualsiasi altra classe del filesplitter:
 * 1. BYTES_IN_KB : numero di bytes presenti in 1 kb
 * 2. KEY : chiave usata per criptare e decriptare i file
 * 3. ALGORITHM : algoritmo (stringa) usato per criptare e decriptare
 * 4. TRANSFORMATION : stringa usata per istanziare il cifratore Cipher
 */
public class Constants {
    public static int BYTES_IN_KB = 1024;
    public static String KEY = "ThisIs@S3cr3tKEY";
    public static String ALGORITHM = "AES";
    public static String TRANSFORMATION = "AES/ECB/PKCS5Padding";
}
