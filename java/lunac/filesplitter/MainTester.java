package lunac.filesplitter;

import lunac.filesplitter.GUI.MainFrame;
import lunac.filesplitter.GUI.MainPanel;

import java.awt.*;


/**
 * Classe MainTester contenente il main:
 * mostra il frame con il pannello contenente la maschera del programma con cui l'utente puo' interagire
 */
public class MainTester {
    public static void main(String[] args) {

        // java - get screen size using the Toolkit class
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

        MainPanel panel = new MainPanel();  //Creo un pannello
        MainFrame mainFrame = new MainFrame(); //Creo un frame

        mainFrame.setLocation(screenSize.width/3, (screenSize.height-panel.getPreferredSize().height)/3);
        mainFrame.setPreferredSize(new Dimension(900, 900));
        mainFrame.setVisible(true);
        mainFrame.add(panel);   //Aggiungo il pannello al frame
        mainFrame.pack();

    }
}
