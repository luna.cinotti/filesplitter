package lunac.filesplitter;

/**
 * Classe enumerato che classifica le modalità di splitter
 */
public enum SplitterTypes {
    Default("DEFAULT"),
    Crypt("CRYPT"),
    Compress("COMPRESS"),
    Parts("NUMBER_PARTS");

    private String splitter;

    /**
     * Costruttore per istanziare un enumerato di tipo SplitterTypes
     * @param splitter tipo splitter come stringa
     */
    SplitterTypes(String splitter) {
        this.splitter = splitter;
    }

    /**
     * Metodo che restituisce la stringa corrispondente al tipo di splitter selezionato
     * @return tipo splitter (String)
     */
    public String getSplitter() {
        return splitter;
    }
}
