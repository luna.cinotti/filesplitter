package lunac.filesplitter.split_manager;

import lunac.filesplitter.Constants;

import java.io.*;
import java.util.zip.DeflaterOutputStream;

/**
 * Classe che estende lo splittamento base del file in parti che vengono compresse
 */
public class CompressedSplitter extends DefaultSplitter {

    /**
     * Costruttore con parametri:
     * @param file file originale da dividere
     * @param dimParts dimensione di ogni parte
     * @param extension estensione dei file compressi
     */
    public CompressedSplitter(File file, long dimParts, String extension) {
        super(file, dimParts, extension);
    }

    /**
     * Sovrascrizione del metodo split dove avviene la compressione di ogni parte generata
     * @param is InputStream che incapsula il file originale
     * @param os OutputStream incapsulato in DeflaterOutputStream per la compressione dei dati in scrittura
     * @param buf buffer di 1 kb (= 1024 bytes)
     * @param bytesRead numero di bytes letti nel buffer
     * @param totalBytesRead totale bytes letti, che si azzera ad ogni ciclo
     * @throws IOException errore IO
     */
    @Override
    public void split(InputStream is, OutputStream os, byte[] buf, int bytesRead, int totalBytesRead) throws IOException {
        //assegno FileOutputStream a DeflaterOutputStream per comprimere i dati in scrittura
        DeflaterOutputStream dos = new DeflaterOutputStream(os);

        while (totalBytesRead < getDimParts() && (bytesRead = is.read(buf)) > 0) {
            //Writes bytes from the specified byte array
            //starting at offset to this buffered output stream.
            dos.write(buf, 0, bytesRead);
            dos.flush();                     //aggiorno la dimensione di ogni file creato

            totalBytesRead += bytesRead;    //aggiorno i bytes totali letti

            if (getDimParts() > Constants.BYTES_IN_KB && totalBytesRead + getLastBytesEachParts() == getDimParts()) {
                buf = new byte[(int)getLastBytesEachParts()];    //aggiorno la dimensione del buf con i bytes rimanenti
            }
        }
        dos.close(); //chiusura file creato
    }
}
