package lunac.filesplitter.split_manager;

import lunac.filesplitter.Constants;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

/**
 * Classe che estende la modalita' di Default di splittamento del file generando parti criptate
 */
public class CryptedSplitter extends DefaultSplitter {

    /**
     * Costruttore con parametri:
     * @param file file originale da dividere
     * @param dimParts dimensione di ogni parte
     * @param extension estensione dei file criptati
     */
    public CryptedSplitter(File file, long dimParts, String extension) {
        super(file, dimParts, extension);
    }

    /**
     * Sovrascrizione del metodo split che genera file criptati tramite un Cipher che utilizza una chiave segreta e un algoritmo
     * @param is InputStream che incapsula il file originale
     * @param os OutputStream che viene a sua volta incapsulato da CipherOutputStream che crypta i dati in scrittura
     * @param buf buffer di 1 kb (= 1024 bytes)
     * @param bytesRead numero di bytes letti nel buffer
     * @param totalBytesRead totale bytes letti, che si azzera ad ogni ciclo
     * @throws IOException errore IO
     */
    @Override
    public void split(InputStream is, OutputStream os, byte[] buf, int bytesRead, int totalBytesRead) throws IOException {
        try {
            Key secretKey = new SecretKeySpec(Constants.KEY.getBytes(), Constants.ALGORITHM);
            Cipher cipher = Cipher.getInstance(Constants.TRANSFORMATION);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);

            //assegno OutputStream al CipherOutputStream per cryptarlo tramite cipher
            CipherOutputStream cipherOut = new CipherOutputStream(os, cipher);

            while (totalBytesRead < getDimParts() && (bytesRead = is.read(buf)) > 0) {
                //Writes bytes from the specified byte array
                //starting at offset to this buffered output stream.
                cipherOut.write(buf, 0, bytesRead);
                cipherOut.flush();                     //aggiorno la dimensione di ogni file creato

                totalBytesRead += bytesRead;    //aggiorno i bytes totali letti

                if ( (getDimParts() > Constants.BYTES_IN_KB) &&
                        ((totalBytesRead + getLastBytesEachParts()) == getDimParts()) )
                    buf = new byte[(int)getLastBytesEachParts()];    //aggiorno la dimensione del buf con i bytes rimanenti
            }
            cipherOut.close(); //chiusura file criptato
            os.close();        //chiusura OutputStream

        } catch (NoSuchPaddingException | NoSuchAlgorithmException
                | InvalidKeyException e) {
            e.printStackTrace();
        }
    }

}
