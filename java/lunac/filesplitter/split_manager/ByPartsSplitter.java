package lunac.filesplitter.split_manager;

import lunac.filesplitter.Constants;

import java.io.*;

/**
 * Classe che estende DefaultSplitter: il file viene diviso conoscendo il numero di parti
 */
public class ByPartsSplitter extends DefaultSplitter {

    /**
     * Costruttore con parametri:
     * @param file file originale da dividere
     * @param dimParts numero di parti specificato dall'utente
     * @param extension estensione assegnata ai file generati
     */
    public ByPartsSplitter(File file, long dimParts, String extension) {
        super(file, dimParts, extension);
        setParts(getDimParts());
        setDimParts(file.length() / getParts());
        setDimLastPart(getDimParts() + ((file.length() % getParts()) * getParts()));
        setLastBytesEachParts(getDimParts() % Constants.BYTES_IN_KB);
    }

}
