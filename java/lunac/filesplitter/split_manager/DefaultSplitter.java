package lunac.filesplitter.split_manager;

import lunac.filesplitter.Constants;

import java.io.*;

/**
 * Classe che genera la divisione di un file in piu' parti conoscendo la dimensione
 * che deve avere ogni parte (modalita' splitter di default)
 */
public class DefaultSplitter {
    private File file;                  //file da splittare
    private String extension;           //estensione da assegnare alle parti del file splittato
    private long dimParts;              //dimensione che deve avere ogni parte
    private long dimLastPart;           //dimensione che deve avere l'ultima parte
    private long parts;                 //numero di parti in cui splittare il file
    private long lastBytesEachParts;    //ultimi bytes (< 1 KB) da leggere alla fine di ogni parte

    /**
     * Costruttore con parametri:
     * @param file file originale da dividere
     * @param dimParts dimensione che deve avere ogni parte, specificata dall'utente nella tabella
     * @param extension estensione da assegnare ad ogni parte generata
     */
    public DefaultSplitter(File file, long dimParts, String extension) {
        this.file = file;
        this.extension = extension;
        this.dimParts = dimParts;
        parts = (file.length() / dimParts) + 1;
        dimLastPart = file.length() % dimParts;
        lastBytesEachParts = dimParts % Constants.BYTES_IN_KB;
    }

    /**
     * Metodo che restituisce il file originale
     * @return File
     */
    public File getFile() {
        return file;
    }

    /**
     * Metodo per modificare il file originale
     * @param file oggetto di tipo File
     */
    public void setFile(File file) {
        this.file = file;
    }

    /**
     * Metodo che restituisce l'estensione
     * @return oggetto di tipo String
     */
    public String getExtension() {
        return extension;
    }

    /**
     * Metodo per modificare l'estensione
     * @param extension oggetto di tipo String
     */
    public void setExtension(String extension) {
        this.extension = extension;
    }

    /**
     * Metodo che restituisce la dimensione delle parti
     * @return valore numerico di tipo long
     */
    public long getDimParts() {
        return dimParts;
    }

    /**
     * Metodo per modificare la dimensione delle parti
     * @param dimParts valore numerico di tipo long
     */
    public void setDimParts(long dimParts) {
        this.dimParts = dimParts;
    }

    /**
     * Metodo che restituisce la dimensione che deve avere l'ultima parte (bytes rimanenti dalla divisione)
     * @return valore numerico di tipo long
     */
    public long getDimLastPart() {
        return dimLastPart;
    }

    /**
     * Metodo per modificare la dimensione dell'ultima parte
     * @param dimLastPart valore numerico di tipo long
     */
    public void setDimLastPart(long dimLastPart) {
        this.dimLastPart = dimLastPart;
    }

    /**
     * Metodo che restituisce il numero di parti con cui viene diviso il file
     * @return valore numerico di tipo long
     */
    public long getParts() {
        return parts;
    }

    /**
     * Metodo per modificare il numero di parti
     * @param parts valore numerico di tipo long
     */
    public void setParts(long parts) {
        this.parts = parts;
    }

    /**
     * Metodo che restituisce i bytes terminali di ogni parte (valore minore di 1 kb)
     * @return valore numerico di tipo long
     */
    public long getLastBytesEachParts() {
        return lastBytesEachParts;
    }

    /**
     * Metodo per modificare i bytes terminali di ogni parte
     * @param lastBytesEachParts valore numerico di tipo long
     */
    public void setLastBytesEachParts(long lastBytesEachParts) {
        this.lastBytesEachParts = lastBytesEachParts;
    }

    /**
     * Metodo che processa la divisione del file in tot parti tramite un ciclo
     */
    public void processSplit() {
        File dest = null;
        InputStream is = null;
        OutputStream os = null;

        int count = 1;  //contatore parti del file che verranno creati
        try {
            is = new FileInputStream(file);

            while (count <= parts) {
                dest = new File(file.getAbsolutePath() + "." + count + extension);
                os = new FileOutputStream(dest);

                byte[] buf = new byte[Constants.BYTES_IN_KB];   //buffer di 1 KB

                //se la dimensione di ogni parte è < 1 KB
                //aggiorno la dimensione del buf con la dimensione del file
                if (count == parts) {
                    dimParts = dimLastPart;
                    lastBytesEachParts = dimParts % Constants.BYTES_IN_KB;
                }

                int bytesRead = 0; //bytes letti nel file
                int totalBytesRead = 0; //bytes totali letti che non dovranno superare dimParts

                split(is, os, buf, bytesRead, totalBytesRead);

                count++;    //incremento il contatore
            }
            is.close(); //chiusura file in ingresso
        } catch(IOException e) {
            e.getStackTrace();
        }
    }

    /**
     * Metodo in cui avviene la lettura del file mediante l'uso di un buffer di un 1 kb, fino al raggiungimento della
     * dimensione desiderata per ogni parte generata, e fino a esaurimento con l'ultima parte che conterra' i bytes rimanenti
     * @param is InputStream che incapsula il file originale
     * @param os OutputStream che incapsula ogni parte che viene generata ad ogni ciclo
     * @param buf buffer di 1 kb (= 1024 bytes)
     * @param bytesRead numero di bytes letti nel buffer
     * @param totalBytesRead totale bytes letti, che si azzera ad ogni ciclo
     * @throws IOException errore IO
     */
    public void split(InputStream is, OutputStream os, byte[] buf, int bytesRead, int totalBytesRead) throws IOException {

        while (totalBytesRead < dimParts && (bytesRead = is.read(buf)) > 0) {
            //Writes bytes from the specified byte array
            //starting at offset to this buffered output stream.
            os.write(buf, 0, bytesRead);
            os.flush();                     //aggiorno la dimensione di ogni file creato

            totalBytesRead += bytesRead;    //aggiorno i bytes totali letti

            if (dimParts > Constants.BYTES_IN_KB && totalBytesRead + lastBytesEachParts == dimParts) {
                buf = new byte[(int)lastBytesEachParts];    //aggiorno la dimensione del buf con i bytes rimanenti
            }
        }
        //System.out.println("total bytes read: " + totalBytesRead);
        os.close(); //chiusura file creato
    }
}
