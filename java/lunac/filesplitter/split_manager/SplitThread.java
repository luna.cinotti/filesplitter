package lunac.filesplitter.split_manager;

import javax.swing.*;
import java.util.logging.Logger;

/**
 * Classe SplitThread che implementa l'interfaccia Runnable
 */
public class SplitThread implements Runnable {
    private static final Logger LOGGER = Logger.getLogger(SplitThread.class.getName());
    private DefaultSplitter def;
    private JProgressBar progressBar;
    private int progress;

    /**
     * Costruttore con parametri:
     * @param def modalita' splitter
     * @param progressBar barra di avanzamento dello splitter
     * @param progress  percentuale di avanzamento della barra
     */
    public SplitThread(DefaultSplitter def, JProgressBar progressBar, int progress) {
        this.def = def;
        this.progressBar = progressBar;
        this.progress = progress;
    }

    /**
     * Sovrascrizione del metodo run() della classe Thread:
     * esegue un determinato split, aggiorna la barra di avanzamento ed elimina il file originale
     */
    @Override
    public void run() {
        LOGGER.info("Avvio thread");

        def.processSplit();
        progressBar.setValue(progress);
        def.getFile().delete();
    }
}
