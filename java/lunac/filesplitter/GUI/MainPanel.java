package lunac.filesplitter.GUI;

import lunac.filesplitter.ExtensionTypes;
import lunac.filesplitter.SplitterTypes;
import lunac.filesplitter.merge_manager.*;
import lunac.filesplitter.split_manager.*;

import javax.swing.*;
import javax.swing.table.TableColumn;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Classe dove viene gestita la maschera del programma con cui interagisce l'utente
 */
public class MainPanel extends JPanel implements ActionListener {
    private JButton addFileButton, removeFileButton, changeFileButton;
    private JButton splitButton, mergeButton;
    private JProgressBar progressBar;
    private TableModel model;
    private JTable table;
    private JFileChooser fileChooser;
    private List<TableRow> fileList;

    private static final Logger LOGGER = Logger.getLogger(MainPanel.class.getName());

    /**
     * Costruttore del pannello principale
     */
    public MainPanel() {
        super();

        setLayoutMainPanel();   //setto layout e dimensioni del pannello principale

        JPanel tablePanel = new JPanel(), buttonsPanel = new JPanel();
        JPanel topButtons = new JPanel(), bottomButtons = new JPanel();

        BoxLayout topButtonsBox = new BoxLayout(topButtons, BoxLayout.LINE_AXIS);
        BoxLayout bottomButtonsBox = new BoxLayout(bottomButtons, BoxLayout.LINE_AXIS);

        tablePanel.setLayout(new BoxLayout(tablePanel, BoxLayout.PAGE_AXIS));
        buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.PAGE_AXIS));
        topButtons.setLayout(topButtonsBox);
        bottomButtons.setLayout(bottomButtonsBox);

        fileList = new LinkedList<>();
        fileChooser = new JFileChooser();
        fileChooser.setMultiSelectionEnabled(true);

        model = new TableModel(fileList);
        table = new JTable(model);
        tablePanel.add(new JScrollPane(table));
        setLayoutTable();

        addFileButton = new JButton(ButtonTypes.AddFile.getType());
        addFileButton.addActionListener(this);
        topButtons.add(addFileButton);
        addSpaceBetweenPanels(topButtons, topButtonsBox.getAxis(), 60);

        changeFileButton = new JButton(ButtonTypes.ChangeFile.getType());
        changeFileButton.addActionListener(this);
        topButtons.add(changeFileButton);
        addSpaceBetweenPanels(topButtons, topButtonsBox.getAxis(), 60);

        removeFileButton = new JButton(ButtonTypes.RemoveFile.getType());
        removeFileButton.addActionListener(this);
        topButtons.add(removeFileButton);

        addSpaceBetweenPanels(bottomButtons, bottomButtonsBox.getAxis(), 15);
        splitButton = new JButton(ButtonTypes.SplitButton.getType());
        splitButton.addActionListener(this);
        bottomButtons.add(splitButton);
        addSpaceBetweenPanels(bottomButtons, bottomButtonsBox.getAxis(), 15);

        progressBar = new JProgressBar();
        progressBar.setIndeterminate(false);
        progressBar.setStringPainted(true);
        progressBar.setValue(0);
        bottomButtons.add(progressBar);
        addSpaceBetweenPanels(bottomButtons, bottomButtonsBox.getAxis(), 15);

        mergeButton = new JButton(ButtonTypes.MergeButton.getType());
        mergeButton.addActionListener(this);
        bottomButtons.add(mergeButton);
        addSpaceBetweenPanels(bottomButtons, bottomButtonsBox.getAxis(), 15);

        //aggiungo i vari pannelli a quello principale
        add(tablePanel);
        addSpaceBetweenPanels(tablePanel, BoxLayout.PAGE_AXIS, 15);
        buttonsPanel.add(topButtons);
        addSpaceBetweenPanels(buttonsPanel, BoxLayout.PAGE_AXIS, 15);
        buttonsPanel.add(bottomButtons);
        add(buttonsPanel);
        addSpaceBetweenPanels(buttonsPanel, BoxLayout.PAGE_AXIS, 15);
    }

    /**
     * Metodo di ascolto al click di un determinato bottone
     * @param e evento in ingresso
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        Object button = e.getSource();
        if (addFileButton.equals(button)) {
            int option = fileChooser.showOpenDialog(MainPanel.this);

            if (option == JFileChooser.APPROVE_OPTION) {
                for (File file : fileChooser.getSelectedFiles()) {
                    TableRow element = new TableRow(file, SplitterTypes.Default);
                    fileList.add(element);  //aggiungo un elemento nella lista
                    model.addRow(); //aggiorno la tabella aggiungendo una riga per il file scelto
                }
            }
        }
        if (removeFileButton.equals(button)) {
            if (fileList.size() == 0) {
                ifTableIsEmpty();
                return;
            }

            List<TableRow> rowsList = new LinkedList<>();
            for (int indexRow : table.getSelectedRows()) {
                rowsList.add(fileList.get(indexRow));
                model.deleteRow(indexRow);
            }
            for (TableRow row : rowsList) {
                //LOGGER.info("Dim lista (prima): " + fileList.size());
                fileList.remove(row);
                //LOGGER.info("Dim lista (dopo): " + fileList.size());
            }
        }
        if (changeFileButton.equals(button)) {
            if (fileList.size() == 0) {
                ifTableIsEmpty();
                return;
            }

            int indexRow = table.getSelectedRow();  //indice della riga selezionata
            int option = fileChooser.showOpenDialog(MainPanel.this);

            if (option == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getSelectedFile();

                TableRow newRow = new TableRow(file, SplitterTypes.Default); //nuova riga
                //LOGGER.info("File lista (prima): " + fileList.get(indexRow).getFile().getName());
                fileList.set(indexRow, newRow);  //aggiorno la lista con la nuova riga
                //LOGGER.info("File lista (dopo): " + fileList.get(indexRow).getFile().getName());
                model.updateRow(indexRow);       //aggiorno la tabella
            }
        }
        if (splitButton.equals(button)) {
            if (fileList.size() == 0) {
                ifTableIsEmpty();
                return;
            }

            //controllo che i valori nell'ultima colonna siano corretti altrimenti non procede con lo split
            if (!validateValuesColumn()) {
                return;
            }

            //aggiorno la dimensione del progressBar con la dimensione della lista
            progressBar.setMaximum(fileList.size());

            int numRow = 0; //indice riga della tabella
            while (numRow < model.getRowCount()) {
                TableRow row = fileList.get(numRow);

                long dimParts = Long.parseLong(row.getValue());
                File file = row.getFile();

                DefaultSplitter def = null;
                if (row.getComboBoxOption().equals(SplitterTypes.Default))
                    def = new DefaultSplitter(file, dimParts, ExtensionTypes.Default.getExt());
                if (row.getComboBoxOption().equals(SplitterTypes.Compress))
                    def = new CompressedSplitter(file, dimParts, ExtensionTypes.Compr.getExt());
                if (row.getComboBoxOption().equals(SplitterTypes.Crypt))
                    def = new CryptedSplitter(file, dimParts, ExtensionTypes.Crypt.getExt());
                if (row.getComboBoxOption().equals(SplitterTypes.Parts))
                    def = new ByPartsSplitter(file, dimParts, ExtensionTypes.Parts.getExt());

                int percentProgressBar = ((numRow+1)*100)/fileList.size(); //progresso percentuale sul totale dei file

                Thread thread = new Thread(new SplitThread(def, progressBar, percentProgressBar));
                thread.start();     //avvio il thread

                numRow++; //incremento il contatore delle righe
            }

            model.clearTable(); //svuoto la tabella e la lista dei file
            JOptionPane.showMessageDialog(null, "Splittamento dei file terminato!");

            //resetto a 0 il valore del progressBar
            progressBar.setValue(0);
        }
        if (mergeButton.equals(button)) {
            int option = fileChooser.showOpenDialog(MainPanel.this);
            if (option == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getSelectedFile();

                File dir = new File(file.getParent());                //cartella contenente il file
                File[] files;

                String[] partsName = file.getName().split("\\.");   //parti nome file divisi dal punto
                String number = partsName[partsName.length - 2];
                String ext = "." + partsName[partsName.length - 1];

                if (!number.equals("1"))
                    JOptionPane.showMessageDialog(null, "Selezionare la prima parte di un file splittato");
                else {
                    //creo un array di file contenente le parti splittate
                    files = dir.listFiles((dir1, name) -> name.startsWith((partsName[0])) && name.endsWith(ext));

                    files = sortFiles(files);   //ordino l'array di file
                    //for (File element : files) System.out.println(element.getName()); //stampo i file dell'array

                    DefaultMerger def = null;
                    try {
                        if (ext.equals(ExtensionTypes.Default.getExt()) || ext.equals(ExtensionTypes.Parts.getExt()) )
                            def = new DefaultMerger(files, partsName[0], partsName[1], dir.getAbsolutePath());
                        if (ext.equals(ExtensionTypes.Compr.getExt()) )
                            def = new DecompressedMerger(files, partsName[0], partsName[1], dir.getAbsolutePath());
                        if (ext.equals(ExtensionTypes.Crypt.getExt()) )
                            def = new DecryptedMerger(files, partsName[0], partsName[1], dir.getAbsolutePath());

                        def.processMerger();

                        for (File element : files) element.delete();
                        JOptionPane.showMessageDialog(null, "Merge completato!");

                    } catch (NullPointerException np) {
                        LOGGER.warning("Impossibile eseguire il merge su un puntatore nullo");
                        np.getStackTrace();
                    }
                }
            }
        }
    }

    /**
     * Metodo che restituisce l'array di file ordinato
     * @param files array di file originale
     * @return array ordinato
     */
    public File[] sortFiles(File[] files) {
        File[] array = new File[files.length];

        for (File file : files) {
            String[] partsName = file.getName().split("\\.");   //parti nome file divisi dal punto
            int number = Integer.parseInt(partsName[partsName.length - 2]);
            array[number - 1] = file;
        }
        return array;
    }

    /**
     * Metodo che setta il tipo dei valori nella terza colonna (modalita' splittamento)
     * @param table tabella JTable
     */
    public void setUpTypesColumn(JTable table) {
        JComboBox<SplitterTypes> comboBox = new JComboBox<>();
        comboBox.setEditable(true);
        comboBox.addItem(SplitterTypes.Default);
        comboBox.addItem(SplitterTypes.Crypt);
        comboBox.addItem(SplitterTypes.Compress);
        comboBox.addItem(SplitterTypes.Parts);

        TableColumn optionComboBox = table.getColumnModel().getColumn(2);
        optionComboBox.setCellEditor(new DefaultCellEditor(comboBox));
    }

    /**
     * Metodo che setta il tipo dei valori nella ultima colonna della tabella (dimensione o numero di ogni parte splittata)
     * @param table tabella di tipo JTable
     */
    public void setUpValuesColumn(JTable table) {
        JTextField numberField = new JTextField(25);
        numberField.setEditable(true);

        TableColumn valuesColumn = table.getColumnModel().getColumn(3);
        valuesColumn.setCellEditor(new DefaultCellEditor(numberField));
    }

    /**
     * Metodo che setta il layout della tabella
     */
    public void setLayoutTable() {
        table.setRowHeight(20);
        model.setColumnWidth(table.getColumnModel().getColumn(0), 250);
        model.setColumnWidth(table.getColumnModel().getColumn(1), 100);
        setUpTypesColumn(table);
        setUpValuesColumn(table);
    }

    /**
     * Metodo che setta il layout del pannello principale
     */
    public void setLayoutMainPanel() {
        //layout pannello
        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

        //dimensioni pannello
        this.setPreferredSize(new Dimension(700, 700));
    }

    /**
     * Metodo che aggiunge spazio tra gli oggetti del pannello principale
     * @param panel oggetto JPanel a cui aggiungere la distanza
     * @param dir direzione orizzontale o verticale dello spazio
     * @param length distanza (valore intero)
     */
    public void addSpaceBetweenPanels(JPanel panel, int dir, int length) {
        if (dir == BoxLayout.LINE_AXIS)
            panel.add(Box.createRigidArea(new Dimension(length, 0)));
        if (dir == BoxLayout.PAGE_AXIS)
            panel.add(Box.createRigidArea(new Dimension(0, length)));
    }

    /**
     * Metodo che genera un avviso in caso di tabella vuota
     */
    public void ifTableIsEmpty() {
        JOptionPane.showMessageDialog(null, "Attenzione: tabella vuota",
                "Warning",JOptionPane.WARNING_MESSAGE);
    }

    /**
     * Metodo che restituisce true se la stringa è numerica, false altrimenti
     * @param str stringa
     * @return valore booleano
     */
    public boolean isNumeric(String str) {
        if (str == null)
            return false;

        try{
            long n = Long.parseLong(str);
        } catch(NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    /**
     * Metodo che restituisce true se i valori nell'utilma colonna della tabella sono stati inseriti correttamente,
     * false altrimenti
     * @return valore booleano
     */
    public boolean validateValuesColumn() {
        int numRow = 0;
        while (numRow < model.getRowCount()) {
            TableRow row = fileList.get(numRow);
            String value = row.getValue();

            if (!isNumeric(value))
                return false;

            long dimParts = Long.parseLong(value);
            if (dimParts == 0 || dimParts < 0) {
                JOptionPane.showMessageDialog(null, "Inserire un numero positivo " +
                        "nell'ultima colonna riga " + (numRow + 1), "Warning", JOptionPane.WARNING_MESSAGE);
                return false;
            }
            if (dimParts > row.getFile().length()) {
                JOptionPane.showMessageDialog(null, "Inserire un valore minore della dimensione del file " +
                        "nell'ultima colonna riga " + (numRow + 1), "Warning", JOptionPane.WARNING_MESSAGE);
                return false;
            }

            numRow++;
        }
        return true;
    }
}
