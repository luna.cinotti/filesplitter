package lunac.filesplitter.GUI;

/**
 * Classe enumerato che classifica la tipologia di bottoni
 */
public enum ButtonTypes {
    AddFile("Add file"),
    RemoveFile("Remove file"),
    ChangeFile("Change file"),
    SplitButton("Split"),
    MergeButton("Merge");

    private String type;

    /**
     * Costruttore di tipo ButtonTypes
     * @param type nome del bottone (stringa)
     */
    ButtonTypes(String type) {
        this.type = type;
    }

    /**
     * Restituisce il nome di un determinato bottone
     * @return stringa
     */
    public String getType() {
        return type;
    }
}
