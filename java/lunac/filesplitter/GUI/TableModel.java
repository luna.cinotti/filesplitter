package lunac.filesplitter.GUI;

import lunac.filesplitter.SplitterTypes;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import java.util.List;

/**
 * Classe che estende il modello base di una tabella
 */
public class TableModel extends AbstractTableModel {
    private String[] colHeaders = {"NAME FILE", "DIMENSION (BYTES)", "SPLITTER MODE", "PARTS/BYTES"};
    private List<TableRow> fileList;

    /**
     * Costruttore con paramentri:
     * @param list lista dei file di tipo TableRow
     */
    public TableModel(List<TableRow> list) {
        super();
        fileList = list;
    }

    /**
     * Metodo che restituisce il numero di righe della tabella
     * @return dimensione della lista dei file (valore intero)
     */
    @Override
    public int getRowCount() {
        return fileList.size();
    }

    /**
     * Metodo che restituisce il numero di colonne della tabella
     * @return valore intero
     */
    @Override
    public int getColumnCount() {
        return colHeaders.length;
    }

    /**
     * Metodo che restituisce il nome di una colonna dato l'indice
     * @param col indice colonna
     * @return stringa del nome
     */
    @Override
    public String getColumnName(int col) {
        return colHeaders[col];
    }

    /**
     * Metodo che restituisce il valore presente in una cella dato l'indice di riga e di colonna
     * @param rowIndex indice riga
     * @param columnIndex indice colonna
     * @return
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        TableRow row = fileList.get(rowIndex);

        switch (columnIndex) {
            case 0:
                return row.getFile().getName();
            case 1:
                return row.getFile().length();
            case 2:
                return row.getComboBoxOption();
            case 3:
                return row.getValue();
        }
        return null;
    }

    /**
     * Metodo per modificare una cella della tabella
     * @param aValue oggetto da assegnare
     * @param rowIndex indice riga
     * @param columnIndex indice colonna
     */
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        TableRow row = fileList.get(rowIndex);
        if (columnIndex == 2) {
            row.setComboBox((SplitterTypes) aValue);
        }
        if (columnIndex == 3)
           row.setValue((String)aValue);
        this.fireTableCellUpdated(rowIndex,columnIndex);
    }

    /**
     * Determina la classe del valore inserito nella colonna
     * @param c numero colonna
     * @return Classe del valore inserito in quella colonna
     */
    @Override
    public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }

    /**
     * Modifica la larghezza di una colonna
     * @param col colonna
     * @param width larghezza
     */
    public void setColumnWidth(TableColumn col, int width) {
        col.setPreferredWidth(width);
    }

    /**
     * Aggiunge una nuova riga e aggiorna la tabella
     */
    public void addRow() {
        this.fireTableRowsInserted(fileList.size() - 1, fileList.size() - 1);
    }

    /**
     * Rimuove una determinata riga e aggiorna la tabella
     * @param index indice riga
     */
    public void deleteRow(int index) { this.fireTableRowsDeleted(index, index); }

    public void updateRow(int index) { this.fireTableRowsUpdated(index, index); }

    /**
     * Elimina tutte le righe della griglia e i file all'interno della lista
     */
    public void clearTable() {
        this.fireTableRowsDeleted(0, fileList.size() - 1);
        fileList.clear();
    }


    /**
     * Restituisce true se la cella [row, col] è modificabile, altrimenti false
     * @param row indice riga
     * @param col indice colonna
     * @return valore booleano
     */
    public boolean isCellEditable(int row, int col) {
        //Note that the data/cell address is constant,
        //no matter where the cell appears onscreen.
        if (col < 2)
            return false;
        else
            return true;
    }



}
