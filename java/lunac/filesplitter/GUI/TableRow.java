package lunac.filesplitter.GUI;

import lunac.filesplitter.SplitterTypes;

import java.io.File;

/**
 * Classe che contiene i dati di ogni riga della tabella
 */
public class TableRow {
    private File file;
    private SplitterTypes comboBox;
    private String value;

    /**
     * Costruttore con parametri:
     * @param file file
     * @param comboBox opzione di splittamento
     */
    public TableRow(File file, SplitterTypes comboBox) {
        this.file = file;
        setComboBox(comboBox);
        setValue("0");
    }

    /**
     * Metodo che restituisce il file
     * @return File
     */
    public File getFile() {
        return file;
    }

    /**
     * Metodo per modificare il file
     * @param file oggetto di tipo File
     */
    public void setFile(File file) {
        this.file = file;
    }

    /**
     * Metodo che restituisce la dimensione o il numero delle parti
     * @return stringa numerica
     */
    public String getValue() {
        return value;
    }

    /**
     * Metodo per modificare la dimensione o il numero delle parti
     * @param textField stringa numerica
     */
    public void setValue(String textField) {
        this.value = textField;
    }

    /**
     * Restituisce l'opzione di splittamento selezionata dall'utente nell'elenco a discesa
     * @return oggetto di tipo SplitterTypes
     */
    public SplitterTypes getComboBoxOption() {
        return comboBox;
    }

    /**
     * Metodo per modificare l'opzione di splittamento desiderata
     * @param comboBox oggetto di tipo SplitterTypes
     */
    public void setComboBox(SplitterTypes comboBox) {
        this.comboBox = comboBox;
    }
}
