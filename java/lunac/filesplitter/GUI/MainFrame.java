package lunac.filesplitter.GUI;

import javax.swing.*;

/**
 * Classe che genera la finestra dell'interfaccia grafica
 */
public class MainFrame extends JFrame {

    /**
     * Costruttore del Frame
     */
    public MainFrame(){
        super("FILE SPLITTER");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}
