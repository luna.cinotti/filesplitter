package lunac.filesplitter.merge_manager;

import java.io.*;
import java.util.zip.InflaterInputStream;

/**
 * Classe che estende DeafultMerger per decomprimere le parti precedentemente splittate e compresse
 * ed infine ricomporle generando il file originale
 */
public class DecompressedMerger extends DefaultMerger {

    /**
     * Costruttore con parametri:
     * @param files lista delle parti da unire
     * @param fileName nome del file originale
     * @param extension estensione del file originale
     * @param path percorso in cui generare il file ricomposto
     */
    public DecompressedMerger(File[] files, String fileName, String extension, String path) {
        super(files, fileName, extension, path);
    }

    /**
     * Sovrascrizione del metodo merge per file da decomprimere
     * @param is InputStream incapsulato in InflaterInputStream per decomporre i dati in lettura
     * @param os OutputStream che incapsula il file finale
     * @param buf buffer di 1 kb per la lettura dei byte
     * @param bytesRead numero di byte letti nel buffer
     * @throws IOException eccezione generata in caso di input/output nulli
     */
    @Override
    public void merge (InputStream is, OutputStream os, byte[] buf, int bytesRead) throws IOException {

        //assegno InflaterInputStream al FileInputStream per decomprimere i dati
        InflaterInputStream iis = new InflaterInputStream(is);

        while ((bytesRead = iis.read(buf)) > 0) {
            //Writes bytes from the specified byte array
            //starting at offset to this buffered output stream.
            os.write(buf, 0, bytesRead);
            os.flush();    //aggiorno la dimensione del file creato nella cartella
        }
        iis.close();       //chiusura file letto
    }
}
