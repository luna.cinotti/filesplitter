package lunac.filesplitter.merge_manager;

import lunac.filesplitter.Constants;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

/**
 * Classe che estende il DefaultMerger per decriptare parti precedentemente criptati e successivamente ricomporre il file originale
 */
public class DecryptedMerger extends DefaultMerger {

    /**
     * Costruttore con parametri:
     * @param files lista parti da unire
     * @param fileName nome del file originale
     * @param extension estensione del file originale
     * @param path percorso della cartella dove generare il file ricomposto
     */
    public DecryptedMerger(File[] files, String fileName, String extension, String path) {
        super(files, fileName, extension, path);
    }

    /**
     * Sovrascrizione del metodo di unione per file da decriptare
     * @param is InputStream incapsulato in CipherInputStream per decriptare i dati in lettura
     * @param os OutputStream che incapsula il file finale
     * @param buf buffer di 1 kb per la lettura dei byte
     * @param bytesRead numero di byte letti nel buffer
     * @throws IOException eccezione generata in caso di input/output nulli
     */
    @Override
    public void merge (InputStream is, OutputStream os, byte[] buf, int bytesRead) throws IOException  {
        try {
            Key secretKey = new SecretKeySpec(Constants.KEY.getBytes(), Constants.ALGORITHM);
            Cipher cipher = Cipher.getInstance(Constants.TRANSFORMATION);
            cipher.init(Cipher.DECRYPT_MODE, secretKey);

            //assegno InputStream al CipherInputStream per decriptarlo tramite cipher
            CipherInputStream cipherIn = new CipherInputStream(is, cipher);

            while ((bytesRead = cipherIn.read(buf)) > 0) {
                //Writes bytes from the specified byte array
                //starting at offset to this buffered output stream.
                os.write(buf, 0, bytesRead);
                os.flush();     //aggiorno la dimensione del file creato nella cartella
            }
            cipherIn.close();   //chiusura file letto

        } catch (NoSuchPaddingException | NoSuchAlgorithmException
                | InvalidKeyException e) {
            e.printStackTrace();
        }
    }
}
