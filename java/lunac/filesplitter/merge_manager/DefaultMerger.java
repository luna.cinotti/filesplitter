package lunac.filesplitter.merge_manager;

import lunac.filesplitter.Constants;

import java.io.*;

/**
 * Classe che data la prima parte di un file splittato rigenera il file originale unendo tutte le parti (default merger)
 */
public class DefaultMerger {
    private File[] files;
    private String fileName;
    private String extension;
    private String pathFolder;

    /**
     * Costruttore con parametri:
     * @param files lista delle parti da unire
     * @param fileName nome del file originale
     * @param extension estensione del file originale
     * @param path percorso assoluto in cui generare il file
     */
    public DefaultMerger (File[] files, String fileName, String extension, String path) {
        this.files = files;
        this.fileName = fileName;
        this.extension = extension;
        this.pathFolder = path;
    }

    /**
     * Metodo che restituisce la lista dei file da unire
     * @return array di File
     */
    public File[] getFiles() {
        return files;
    }

    /**
     * Metodo per modificare la lista dei file
     * @param files array di File
     */
    public void setFiles(File[] files) {
        this.files = files;
    }

    /**
     * Metodo che restituisce il nome del file originale
     * @return stringa
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Metodo per modificare il nome del file
     * @param fileName stringa del nome da assegnare al file
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * Metodo che restituisce l'estensione del file in uscita
     * @return stringa
     */
    public String getExtension() {
        return extension;
    }

    /**
     * Metodo per modificare l'estensione del file in uscita
     * @param extension stringa
     */
    public void setExtension(String extension) {
        this.extension = extension;
    }

    /**
     * Metodo che restituisce il percorso assoluto della cartella padre
     * @return stringa
     */
    public String getPathFolder() {
        return pathFolder;
    }

    /**
     * Metodo per modificare il percorso della cartella padre
     * @param pathFolder stringa dell'indirizzo
     */
    public void setPathFolder(String pathFolder) {
        this.pathFolder = pathFolder;
    }

    /**
     * Metodo che processa l'unione delle parti di un file precedentemente splittato tramite un ciclo
     */
    public void processMerger() {
        //file che verrà creato al termine della funzione
        File dest = new File(pathFolder + "\\" + fileName + "." + extension);

        OutputStream os = null;
        InputStream is = null;

        int count = 0;  //contatore file nell'array files
        try {
            os = new FileOutputStream(dest);

            while (count < files.length) {
                is = new FileInputStream(files[count]); //prende ogni file nell'array files

                byte[] buf = new byte[Constants.BYTES_IN_KB];    // buffer di 1 KB

                int bytesRead = 0;  //bytes letti nel file

                merge(is, os, buf, bytesRead);

                count++;    //incremento del contatore
            }
            os.close(); //chiusura file creato
        }
        catch (IOException e) {
            e.getStackTrace();
        }
    }

    /**
     * Metodo in cui avviene la lettura dei dati di ogni parte e la successiva scrittura su un unico file in uscita
     * @param is InputStream che incapsula ogni parte
     * @param os OutputStream che incapsula il file finale
     * @param buf buffer di 1 kb per la lettura dei byte
     * @param bytesRead numero di byte letti nel buffer
     * @throws IOException eccezione generato in caso di input/output nulli
     */
    public void merge (InputStream is, OutputStream os, byte[] buf, int bytesRead) throws IOException {

        while ((bytesRead = is.read(buf)) > 0) {
            //Writes bytes from the specified byte array
            //starting at offset to this buffered output stream.
            os.write(buf, 0, bytesRead);
            os.flush();   //aggiorno la dimensione del file creato nella cartella
        }
        is.close();  //chiusura file letto
    }
}
