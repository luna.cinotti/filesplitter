package lunac.filesplitter;

/**
 * Classe enumerato che classifica le diverse estensioni di un file
 * in base alle modalità di splittamento dello stesso
 */
public enum ExtensionTypes {
    Default(".par"),
    Crypt(".crypt"),
    Compr(".zip"),
    Parts(Default.getExt());

    private String ext;

    /**
     * Costruttore per istanziare un enumerato del tipo ExtensionTypes
     * @param ext estensione del file come stringa
     */
    ExtensionTypes(String ext) {
        this.ext = ext;
    }

    /**
     * Metodo che restituisce una determinata estensione in base alla modalita' di splitter selezionata
     * @return estensione (String)
     */
    public String getExt() {
        return ext;
    }
}
