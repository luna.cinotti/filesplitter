set javacPath="C:\Program Files\Java\jdk1.8.0_241\bin\javac"
set srcPath="C:\Users\cinal\Documents\ProgettiJava\filesplitter\java\lunac\filesplitter"
set classesPath="C:\Users\cinal\Documents\ProgettiJava\filesplitter\classes"

rmdir /q /s %classesPath%
mkdir %classesPath%

%javacPath% %srcPath%\*.java %srcPath%\GUI\*.java %srcPath%\merge_manager\*.java %srcPath%\split_manager\*.java -d %classesPath%